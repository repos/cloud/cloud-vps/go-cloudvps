// Package cloudvps provides an interface to manage web proxies,
// Puppet configuration, and other features specific to Wikimedia Cloud
// VPS.
package cloudvps
