// Package puppet provides an interface to interact with the Cloud VPS
// Puppet ENC API, which makes it possible to assign Puppet roles and
// hiera data for instances, prefixes or for the entire project.
package puppet

import (
	"github.com/gophercloud/gophercloud"
)

// Client interacts with the Cloud VPS Puppet ENC API.
type Client struct {
	Client *gophercloud.ServiceClient
}
