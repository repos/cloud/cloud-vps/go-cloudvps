package puppet

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
)

// Prefix represents a "prefix" (which in reality represents a single
// instance, all instances whose name start with a certain prefix or
// all instances in the entire project) that can have roles and hiera
// data assigned to it.
type Prefix struct {
	// Id is the internal identifier of this prefix.
	Id int `json:"id"`

	// Name is the name of this prefix.
	Name string `json:"prefix"`

	// Roles is the list of Puppet roles assigned to this role.
	Roles []string `json:"roles"`

	// Hiera is the map of hiera data applied to this role.
	Hiera map[string]any `json:"hiera"`
}

type singlePrefixResult struct {
	gophercloud.Result
}

func (r singlePrefixResult) Extract() (*Prefix, error) {
	p := Prefix{}
	err := r.ExtractInto(&p)
	return &p, err
}

type CreatePrefixResult struct {
	singlePrefixResult
}

func (client Client) CreatePrefix(prefix Prefix) (r CreatePrefixResult) {
	url := client.Client.ServiceURL("prefix")

	resp, err := client.Client.Post(url, prefix, &r.Body, &gophercloud.RequestOpts{
		OkCodes: []int{200},
	})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}

type GetPrefixResult struct {
	singlePrefixResult
}

func (client Client) GetPrefixById(id int) (r GetPrefixResult) {
	url := client.Client.ServiceURL("prefix", "id", fmt.Sprintf("%v", id))

	resp, err := client.Client.Get(url, &r.Body, &gophercloud.RequestOpts{OkCodes: []int{200}})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}

type UpdatePrefixResult struct {
	singlePrefixResult
}

func (client Client) UpdatePrefix(prefix Prefix) (r UpdatePrefixResult) {
	url := client.Client.ServiceURL("prefix", "id", fmt.Sprintf("%v", prefix.Id))

	resp, err := client.Client.Put(url, prefix, &r.Body, &gophercloud.RequestOpts{OkCodes: []int{200}})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}

type DeletePrefixResult struct {
	gophercloud.Result
}

func (client Client) DeletePrefix(prefix Prefix) (r DeletePrefixResult) {
	url := client.Client.ServiceURL("prefix", prefix.Name)

	resp, err := client.Client.Delete(url, &gophercloud.RequestOpts{OkCodes: []int{200}})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}
