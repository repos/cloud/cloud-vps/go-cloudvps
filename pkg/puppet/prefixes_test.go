package puppet

import (
	"fmt"
	"net/http"
	"testing"

	th "github.com/gophercloud/gophercloud/testhelper"
	fake "github.com/gophercloud/gophercloud/testhelper/client"
	"github.com/stretchr/testify/assert"
)

func TestGetPrefixById(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	th.Mux.HandleFunc("/prefix/id/1", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "GET")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)
		th.TestHeader(t, r, "Accept", "application/json")

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, `{
	"id": 1,
	"prefix": "foo",
	"roles": ["a", "b", "c"],
	"hiera": {"first": "a", "second": "b"}
		}`)
	})

	puppetClient := Client{Client: fake.ServiceClient()}

	t.Run("Existing prefix", func(t *testing.T) {
		response := puppetClient.GetPrefixById(1)
		th.AssertNoErr(t, response.Err)
		prefix, err := response.Extract()
		th.AssertNoErr(t, err)

		assert.Equal(t, Prefix{
			Id:    1,
			Name:  "foo",
			Roles: []string{"a", "b", "c"},
			Hiera: map[string]any{
				"first":  "a",
				"second": "b",
			},
		}, *prefix)
	})
}
