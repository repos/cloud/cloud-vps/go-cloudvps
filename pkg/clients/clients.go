// Package clients contains constructors to create client objects
// for the service clients implemented by this module.
package clients

import (
	"github.com/gophercloud/gophercloud"

	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/puppet"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/webproxy"
)

func getClient(service string, client *gophercloud.ProviderClient, eo gophercloud.EndpointOpts) (*gophercloud.ServiceClient, error) {
	sc := new(gophercloud.ServiceClient)
	eo.ApplyDefaults(service)

	url, err := client.EndpointLocator(eo)
	if err != nil {
		return nil, err
	}

	sc.ProviderClient = client
	sc.Endpoint = url
	sc.Type = service

	return sc, nil
}

// NewPuppetClient creates a client that can be used to interact with
// the Puppet ENC API.
func NewPuppetClient(client *gophercloud.ProviderClient, eo gophercloud.EndpointOpts) (*puppet.Client, error) {
	sc, err := getClient("puppet-enc", client, eo)
	if err != nil {
		return nil, err
	}

	return &puppet.Client{
		Client: sc,
	}, nil
}

// NewWebProxyClient creates a client that can be used to interact with
// the web proxy API.
func NewWebProxyClient(client *gophercloud.ProviderClient, eo gophercloud.EndpointOpts) (*webproxy.Client, error) {
	sc, err := getClient("proxy", client, eo)
	if err != nil {
		return nil, err
	}

	return &webproxy.Client{
		Client: sc,
	}, nil
}
