package webproxy

import (
	"fmt"
	"net/http"
	"testing"

	th "github.com/gophercloud/gophercloud/testhelper"
	fake "github.com/gophercloud/gophercloud/testhelper/client"
	"github.com/stretchr/testify/assert"
)

const listDomainsResponse = `
{
	"wmcloud.org": {
		"deprecated": false,
		"default": true
	},
	"wmflabs.org": {
		"deprecated": true,
		"default": false
	},
	"example.org": {
		"deprecated": false,
		"default": false
	}
}
`

func TestListDomains(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupListDomainsTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}

	response := wpClient.ListDomains()
	th.AssertNoErr(t, response.Err)

	assert.EqualValues(t,
		[]Domain{
			{Name: "example.org", Default: false, Deprecated: false},
			{Name: "wmcloud.org", Default: true, Deprecated: false},
			{Name: "wmflabs.org", Default: false, Deprecated: true},
		},
		response.Domains())

	th.AssertEquals(t, "wmcloud.org", response.Default().Name)
}

func setupListDomainsTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/zones", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "GET")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, listDomainsResponse)
	})
}
