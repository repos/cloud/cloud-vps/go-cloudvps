package webproxy

import (
	"fmt"
	"net/http"
	"testing"

	th "github.com/gophercloud/gophercloud/testhelper"
	fake "github.com/gophercloud/gophercloud/testhelper/client"
	"github.com/stretchr/testify/assert"
)

const listProxiesResponse = `
{
  "routes": [
    {
      "backends": [
        "http://172.16.128.248:80"
      ],
      "domain": "taavi-test-project.codfw1dev.wmcloud.org"
    },
    {
      "backends": [
        "http://172.16.128.248:8000"
      ],
      "domain": "taavi-test-webproxy.codfw1dev.wmcloud.org"
    }
  ]
}
`

const getProxyFoundResponse = `
{
  "backends": [
	"http://172.16.128.248:80"
  ],
  "domain": "taavi-test-project.codfw1dev.wmcloud.org"
}
`

const createDomainUnavailableResponse = `
{
  "error": "Can't use domain test2.wmcloud.org'"
}
`

func TestListProxies(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupListProxiesTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}

	response := wpClient.ListProxies()
	th.AssertNoErr(t, response.Err)
	proxies, err := response.Proxies()
	th.AssertNoErr(t, err)

	assert.EqualValues(t,
		[]Proxy{
			{
				Domain:         "taavi-test-project.codfw1dev.wmcloud.org",
				originalDomain: "taavi-test-project.codfw1dev.wmcloud.org",
				Backends:       []string{"http://172.16.128.248:80"},
			},
			{
				Domain:         "taavi-test-webproxy.codfw1dev.wmcloud.org",
				originalDomain: "taavi-test-webproxy.codfw1dev.wmcloud.org",
				Backends:       []string{"http://172.16.128.248:8000"},
			},
		},
		proxies)
}

func setupListProxiesTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/mapping", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "GET")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, listProxiesResponse)
	})
}

func TestFindProxy(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupGetProxyTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}

	t.Run("Existing proxy", func(t *testing.T) {
		response := wpClient.FindProxy("taavi-test-project.codfw1dev.wmcloud.org")
		th.AssertNoErr(t, response.Err)
		proxy, err := response.Proxy()
		th.AssertNoErr(t, err)
		assert.Equal(t,
			proxy,
			Proxy{
				Domain:         "taavi-test-project.codfw1dev.wmcloud.org",
				originalDomain: "taavi-test-project.codfw1dev.wmcloud.org",
				Backends:       []string{"http://172.16.128.248:80"},
			})
	})

	t.Run("Non-existent proxy", func(t *testing.T) {
		response := wpClient.FindProxy("doesnt-exist.codfw1dev.wmcloud.org")
		th.AssertErr(t, response.Err)
	})
}

func setupGetProxyTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/mapping/taavi-test-project.codfw1dev.wmcloud.org", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "GET")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, getProxyFoundResponse)
	})

	th.Mux.HandleFunc("/mapping/doesnt-exist.codfw1dev.wmcloud.org", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "GET")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)

		w.Header().Add("Content-Type", "text/plain")
		w.WriteHeader(http.StatusNotFound)

		fmt.Fprintf(w, "No such domain")
	})
}

func TestCreateProxyOk(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupOkCreateProxyTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}
	proxy := Proxy{Domain: "test.example.org", Backends: []string{"http://127.0.0.1:1234"}}
	response := wpClient.CreateProxy(&proxy)
	th.AssertNoErr(t, response.Err)
	th.AssertEquals(t, "test.example.org", proxy.originalDomain)
}

func TestCreateProxyNameUnavailable(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupFailingCreateProxyTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}
	proxy := Proxy{Domain: "test2.example.org", Backends: []string{"http://127.0.0.1:1234"}}
	response := wpClient.CreateProxy(&proxy)
	th.AssertErr(t, response.Err)
}

func setupOkCreateProxyTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/mapping", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "PUT")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)
		th.TestJSONRequest(t, r, `
{
	"domain": "test.example.org",
	"backends": [
		"http://127.0.0.1:1234"
	]
}
`)

		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, "")
	})
}

func setupFailingCreateProxyTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/mapping", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "PUT")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)
		th.TestJSONRequest(t, r, `
{
	"domain": "test2.example.org",
	"backends": [
		"http://127.0.0.1:1234"
	]
}
`)

		w.WriteHeader(http.StatusForbidden)

		fmt.Fprintf(w, createDomainUnavailableResponse)
	})
}

func TestUpdateProxy(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupUpdateProxyTestResponse(t)

	wpClient := Client{Client: fake.ServiceClient()}
	proxy := Proxy{
		Domain:         "test2.example.org",
		originalDomain: "test.example.org",
		Backends:       []string{"http://127.0.0.3:1234"},
	}

	response := wpClient.UpdateProxy(&proxy)
	th.AssertNoErr(t, response.Err)
	th.AssertEquals(t, "test2.example.org", proxy.originalDomain)
}

func setupUpdateProxyTestResponse(t *testing.T) {
	th.Mux.HandleFunc("/mapping/test.example.org", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "POST")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)
		th.TestJSONRequest(t, r, `
{
	"domain": "test2.example.org",
	"backends": [
		"http://127.0.0.3:1234"
	]
}
`)

		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, "OK")
	})
}

func TestDeleteProxy(t *testing.T) {
	th.SetupHTTP()
	defer th.TeardownHTTP()

	setupDeleteProxyTestRespones(t)

	wpClient := Client{Client: fake.ServiceClient()}
	proxy := Proxy{
		Domain:         "test2.example.org",
		originalDomain: "test.example.org",
		Backends:       []string{"http://127.0.0.3:1234"},
	}

	response := wpClient.DeleteProxy(&proxy)
	th.AssertNoErr(t, response.Err)
}

func setupDeleteProxyTestRespones(t *testing.T) {
	th.Mux.HandleFunc("/mapping/test.example.org", func(w http.ResponseWriter, r *http.Request) {
		th.TestMethod(t, r, "DELETE")
		th.TestHeader(t, r, "X-Auth-Token", fake.TokenID)

		w.WriteHeader(http.StatusOK)

		fmt.Fprintf(w, "deleted")
	})
}
