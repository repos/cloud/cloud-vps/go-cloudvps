package webproxy

import (
	"sort"

	"github.com/gophercloud/gophercloud"
)

// Domain represents a domain such as wmcloud.org that is available for
// web proxy usage.
type Domain struct {
	// Name is the DNS domain name of this Domain, without a trailing dot.
	Name string
	// Deprecated is a boolean flag to describe if this domain is
	// deprecated. Only Cloud VPS administrators can use create new
	// proxies with deprecated names.
	Deprecated bool
	// Default is a boolean flag to describe if this domain should be
	// selected by default if no other domain has been selected for a
	// specific web proxy.
	Default bool
}

// DomainResult is the response for a Domain listing.
type DomainResult struct {
	gophercloud.Result
}

// Domains lists the Domain objects in this response.
func (r DomainResult) Domains() []Domain {
	var domains []Domain
	data := r.Body.(map[string]interface{})

	for name, domainData := range data {
		domainDetails := domainData.(map[string]any)
		domains = append(domains, Domain{
			Name:       name,
			Deprecated: domainDetails["deprecated"].(bool),
			Default:    domainDetails["default"].(bool),
		})
	}

	sort.Slice(domains, func(i, j int) bool {
		return domains[i].Name < domains[j].Name
	})

	return domains
}

// Default gets the Domain that has been specified as the default
// selection for new proxies.
func (r DomainResult) Default() *Domain {
	for _, domain := range r.Domains() {
		if domain.Default {
			return &domain
		}
	}

	return nil
}

// ListDomains lists the domains that are currently available for new web proxies.
func (client Client) ListDomains() (r DomainResult) {
	url := client.Client.ServiceURL("zones")

	resp, err := client.Client.Get(url, &r.Body, nil)
	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}
