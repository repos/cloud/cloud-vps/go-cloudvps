// Package webproxy provides an interface to manage Cloud VPS web
// proxies, which provide public HTTPS endpoints for Cloud VPS projects
// without having to allocate floating IP addresses.
package webproxy

import (
	"github.com/gophercloud/gophercloud"
)

// Client interacts with the web proxy API.
type Client struct {
	Client *gophercloud.ServiceClient
}
