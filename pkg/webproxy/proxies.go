package webproxy

import (
	"github.com/gophercloud/gophercloud"
)

// Proxy represents a configured web proxy that provides a public HTTPS
// endpoint for a Cloud VPS instance.
type Proxy struct {
	// Domain is the full DNS name of this proxy.
	Domain string `json:"domain"`

	// originalDomain is for detecting renames when updating/deleting a proxy
	originalDomain string

	// Backends is the list of the HTTP endpoints where traffic will be
	// proxied to.
	Backends []string `json:"backends"`
}

// ProxyListResult is the response for a Proxy listing.
type ProxyListResult struct {
	gophercloud.Result
}

// Proxies lists the Proxy objects in this response.
func (r ProxyListResult) Proxies() ([]Proxy, error) {
	var s struct {
		Routes []Proxy `json:"routes"`
	}
	err := r.ExtractInto(&s)

	for i := range s.Routes {
		s.Routes[i].originalDomain = s.Routes[i].Domain
	}

	return s.Routes, err
}

// ListProxies lists the proxies this project has currently configured.
func (client Client) ListProxies() (r ProxyListResult) {
	url := client.Client.ServiceURL("mapping")

	resp, err := client.Client.Get(url, &r.Body, nil)
	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}

// FindProxyResult is the response for a Proxy query.
type FindProxyResult struct {
	gophercloud.Result
}

// Proxy reads the Proxy object in this response.
func (r FindProxyResult) Proxy() (Proxy, error) {
	var proxy Proxy
	err := r.ExtractInto(&proxy)
	proxy.originalDomain = proxy.Domain
	return proxy, err
}

func (client Client) FindProxy(domain string) (r FindProxyResult) {
	url := client.Client.ServiceURL("mapping", domain)

	resp, err := client.Client.Get(url, &r.Body, nil)
	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	return
}

// CreateProxyResult is the response for creating a Proxy.
type CreateProxyResult struct {
	gophercloud.Result
}

// CreateProxy creates a Proxy.
func (client Client) CreateProxy(proxy *Proxy) (r CreateProxyResult) {
	url := client.Client.ServiceURL("mapping")

	resp, err := client.Client.Put(url, proxy, nil, &gophercloud.RequestOpts{
		OkCodes: []int{200},
	})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	if r.Err == nil {
		proxy.originalDomain = proxy.Domain
	}

	return
}

// UpdateProxyResult is the response for updating a Proxy.
type UpdateProxyResult struct {
	gophercloud.Result
}

// UpdateProxy updates an existing Proxy.
func (client Client) UpdateProxy(proxy *Proxy) (r UpdateProxyResult) {
	url := client.Client.ServiceURL("mapping", proxy.originalDomain)

	resp, err := client.Client.Post(url, proxy, nil, &gophercloud.RequestOpts{
		OkCodes: []int{200},
	})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	if r.Err == nil {
		proxy.originalDomain = proxy.Domain
	}

	return
}

// DeleteProxyResult is the response for deleting a Proxy.
type DeleteProxyResult struct {
	gophercloud.Result
}

// DeleteProxy an existing Proxy.
func (client Client) DeleteProxy(proxy *Proxy) (r DeleteProxyResult) {
	url := client.Client.ServiceURL("mapping", proxy.originalDomain)

	resp, err := client.Client.Delete(url, &gophercloud.RequestOpts{
		OkCodes: []int{200},
	})

	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)

	if r.Err == nil {
		proxy.originalDomain = ""
	}

	return
}
